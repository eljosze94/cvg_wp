<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_cvg');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#M,0jd-DbD=G!2`3/CIow[6;9Lq>}P@vmCZ<vaMqfY4%;A{#-U/48]lo]9l(Dw9Q');
define('SECURE_AUTH_KEY',  'p4j`&NjI*01wjWYR8Zs|mD)Z.=}Rt6#QG]tL8z:O:g}ef/wK#qp#PT}a4Z8A1KC#');
define('LOGGED_IN_KEY',    '/6~b$1vOJoe9.Y*4*VMa[wbjt% V5i%X|HwilvZ$=#ZA@R$kyTRp]ig3/W?I&|=>');
define('NONCE_KEY',        ';A~Fab0*K5ls&HA3@b;59;kS*s:zBF}3|MMzC3znTkf|5(I(+n{k},s>jz@!jo(p');
define('AUTH_SALT',        '^LO;94dT3-ZuRGr7~Z53?q)9mVdc)~R3vAO]|a9lqD%}]l3FNJ:SHJ^&I*d0F5OH');
define('SECURE_AUTH_SALT', '@reoh7ihU{)E|[P6YGq*cuB3lD#qj| O3S9TTs8D,&aEfZhfmK/kY-niI$#`+.,l');
define('LOGGED_IN_SALT',   '~V1;W)Qxe_|gL@M+Bl-|4ao0wh/h*U)3p{mwV`PWS?o4$Xa+4>7.M[@a}!|K44C.');
define('NONCE_SALT',       'D:Ma60!3wSkAt2jyg=#vX7y]?R9*DKQ2JQ;{Y=zd$?NmzWWg#}m[5h)$EFEVd5.I');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
