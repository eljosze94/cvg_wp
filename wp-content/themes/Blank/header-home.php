<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage SUMA
 */
?><!DOCTYPE html>
<html lang="es">

  <head>
	<link rel="shortcut icon" href="img/logos/favicon.png">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ConVerGente</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php bloginfo( 'template_url' ); ?>/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="<?php bloginfo( 'template_url' ); ?>/css/business-casual.css" rel="stylesheet">

  </head>
	<style>
	.img-servicios{
	position: relative;
   	float: left;
	margin: 5px;
	}
	
	.img-servicios span {
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    z-index: 999;
    background: #070b0e82;
    padding: 3px;
	BORDER-RADIUS: 5px 5px 0px 0px;
    color: #fff;
    font-family: sans-serif;
   }
  
	</style>
  <body>