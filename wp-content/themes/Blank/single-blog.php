<?php
get_header('resto'); 
$id = get_the_ID();
setPostViews($id);
?>
<div class="main" role="main">
      <div class="c-inside">
        <div class="c-inside__top"> 
          <h1>  <strong>Blue</strong>Blog  </h1>
        </div>
        <div class="c-blog -single"> 
          <div class="container">
            <div class="row">
              <div class="col-md-12"><!-- <span>Opinion</span> -->
                <h2><?php the_title(); ?></h2>
                <div class="c-blog__body">
                	<?php the_field('contenido');?>
                </div>
                <div class="share"><a class="btn btn-primary" href="javascript:void(window.open('http://www.linkedin.com/shareArticle?url='+encodeURIComponent(location),'','width=600,height=400,left=50,top=50'));">Compartir en Linkedin	</a></div>
              </div>
            </div>
          </div>
        </div>
        <div class="container"> 

          <?php 
          $query = array(
                    'showposts' => 1, 
                    'post_type' =>'blog', 
                    'posts_per_page' => 1,
                    'order'=> 'DESC', 
                    'post__not_in' => array($id ),
                    'orderby' => 'order');
          query_posts($query);
            if ( have_posts() ):
                while (have_posts()) :the_post(); 
                  echo '
          <div class="c-blog -item"> 
            <div class="row">
              <div class="col-md-6"> <img src="'.get_field('imagen').'"></div>
              <div class="col-md-6">
                <div class="c-blog__info"><span>Blog</span>
                  <h5>'.get_the_title().'</h5>
                  <p>'.cortar_palabras(get_field('contenido'),200).'</p>
                  <a class="btn btn-secundary" href="'.get_permalink().'">Ir al artículo</a>
                </div>
              </div>
            </div>
          </div>';
            endwhile;
          endif;
          ?>



        </div>
      </div>
    </div>


<?php get_footer('resto'); ?>


<script type="text/javascript">
$( document ).ready(function() {
	$('.page-service').addClass('page-blog');
	$('.page-service').removeClass('page-service');
});
</script>