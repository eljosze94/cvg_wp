<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
<div class="tagline-upper text-left text-heading text-shadow text-white mt-5 d-none d-lg-block container mb-4"><img src="img/logos/logocvg.png" width="200" height="100"/></div>
    

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-faded py-lg-4">
      <div class="container">
        <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Convergente</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <br>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav mx-auto">
            <li class="nav-item active px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="index.html">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="dropdown nav-item px-lg-4">
				<a class="nav-link text-uppercase text-expanded" data-toggle="dropdown" href="#">Lecturas Reflexivas</span></a>
				<ul class="dropdown-menu">
					<li><a class="nav-link" href="lecturasReflexivas/lecturas/pag1.html" >Lecturas</a></li>
					<li><a class="nav-link" href="lecturasReflexivas/reflexiones/pag1.html" >Reflexiones</a></li>
					<li><a class="nav-link" href="lecturasReflexivas/citasReflexivas/pag1.html">Citas reflexivas</a></li>
				</ul>
            </li>
            <li class="dropdown nav-item px-lg-4">
				<a class="nav-link text-uppercase text-expanded" data-toggle="dropdown" href="#">Recomendamos</span></a>
				<ul class="dropdown-menu">
					<li><a class="nav-link" href="recomendamos/libros/pag1.html" >Libros</a></li>
					<li><a class="nav-link" href="recomendamos/videos/pag1.html">Videos para reflexionar</a></li>
				</ul>
            </li>
            <li class="dropdown nav-item px-lg-4">
				<a class="nav-link text-uppercase text-expanded" data-toggle="dropdown" href="#">Experiencia</span></a>
				<ul class="dropdown-menu">
					<li><a class="nav-link" href="experiencia.html" >Experiencia</a></li>
					<li><a class="nav-link" href="vivencias.html">Vivencias</a></li>
				</ul>
            </li>
			<li class="nav-item px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="about.html">Equipo</a>
            </li>
			<li class="nav-item px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="contact.html">Contacto</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container">

      <div class="bg-faded p-4 my-4">
        <!-- Image Carousel -->
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner" role="listbox">

            <div class="carousel-item active">
              <img class="d-block img-fluid w-100" src="img/slider/slider1.jpg" alt="">
              <div class="carousel-caption d-none d-md-block">
                <h2 class="text-shadow" style="padding: 5px 5px 5px; BACKGROUND-COLOR: #353837a1; border-radius: 5px;">Conversaciones Reflexivas</h2>
                <p class="text-shadow" style="padding: 5px 5px 5px; BACKGROUND-COLOR: #353837a1; border-radius: 5px;">Reflexionaremos sobre nuestras experiencias con las distinciones que vamos viendo en cada sesión y que impactan en la vida familiar, laboral, comercial y comunidad.</p>
              </div>
            </div>
            
            <div class="carousel-item">
              <img class="d-block img-fluid w-100" src="img/slider/slider2.jpg" alt="">
              <div class="carousel-caption d-none d-md-block">
        			  <h1 class="text-shadow">"Lo único permanente es lo que se conserva"</h1>
        				<iframe width="560" height="315" src="https://www.youtube.com/embed/S7ti-gCGCE8?rel=0&showinfo=0"  allowfullscreen></iframe>
        				<h3 class="text-shadow">Todo cambia para conservar lo que se quiere conservar</h3>
              </div>
            </div>
          
            <div class="carousel-item">
              <img class="d-block img-fluid w-100" src="img/fondo1.png"  alt="">
              <div class="carousel-caption d-none d-md-block">
                
                <div class="row mb-2">
                  <div class="col"><img class="img-fluid w-75 img-libros" src="img/slider/libro1.png"></div>
                  <div class="col"><img class="img-fluid w-75 img-libros" src="img/slider/libro2.png"></div>
                  <div class="col"><img class="img-fluid w-75 img-libros" src="img/slider/libro3.png"></div>
                </div>
                <h3 class="text-shadow">Convivir es fruto de nuestra creación. Construimos la sociedad a partir de las relaciones que establecemos</h3>
                <a href="https://www.antartica.cl/antartica/servlet/LibroServlet?action=searchLibrosPorId&orderBy=-1&id=31536&criterio=autor&pagina_actual=1" role="button" class="btn btn-danger btn-lg">Comprar</a>
                <a href="recomendamos/libros/pag1.html" role="button" class="btn btn-primary btn-lg">Saber más</a>
              </div>
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
        <!-- Welcome Message -->
		<!--
        <div class="text-center mt-4">
          <div class="text-heading text-muted text-lg">Welcome To</div>
          <h1 class="my-2">Business Casual</h1>
          <div class="text-heading text-muted text-lg">By
            <strong>Start Bootstrap</strong>
          </div>
        </div>
		-->
      </div>

	  <div class="bg-faded p-4 my-4">
        <h2 class="text-center text-uppercase my-3"><strong>SERVICIOS</strong></h2>
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6 portfolio-item">
					<a class="portfolio-link" data-toggle="modal" href="#modal1">
						<div class="img-servicios">
						  <img class="img-fluid w-100" src="../img/negociacionRelacional.jpg"/>
						  <span align="center">NEGOCIACIÓN RELACIONAL</span>
						</div>
					</a>
				 </div>
				 
				 <div class="col-md-6 col-sm-6 portfolio-item">
					<a class="portfolio-link" data-toggle="modal" href="#modal2">
						<div class="img-servicios">
						  <img class="img-fluid" src="../img/relacionesLaborales.jpg"/>
						  <span align="center">RELACIONES LABORALES Y NEGOCIACIÓN </span>
						</div>
					</a>
				 </div>
				 
				 <div class="col-md-6 col-sm-6 portfolio-item">
					<a class="portfolio-link" data-toggle="modal" href="#modal3">
						<div class="img-servicios">
						  <img class="img-fluid" src="../img/negociacionComercial.jpg"/>
						  <span align="center">NEGOCIACIÓN COMERCIAL Y VENTAS</span>
						</div>
					</a>
				 </div>
				 
				 <div class="col-md-6 col-sm-6 portfolio-item">
					<a class="portfolio-link" data-toggle="modal" href="#modal4">
						<div class="img-servicios">
						  <img class="img-fluid" src="../img/cafeRelacional.jpeg"/>
						  <span align="center">CAFÉ RELACIONAL</span>
						</div>
					</a>
				 </div>
				 
				 <div class="col-md-6 col-sm-6 portfolio-item">
					<a class="portfolio-link" data-toggle="modal" href="#modal5">
						<div class="img-servicios">
						  <img class="img-fluid" src="../img/transformacionCultural.jpg"/>
						  <span align="center">TRANSFORMACIÓN CULTURAL</span>
						</div>
					</a>
				 </div>
				 
				 <div class="col-md-6 col-sm-6 portfolio-item">
					<a class="portfolio-link" data-toggle="modal" href="#modal6">
						<div class="img-servicios">
						  <img class="img-fluid" src="../img/conversacionesReflexivas.png"/>
						  <span align="center">CONVERSACIONES REFLEXIVAS</span>
						</div>
					</a>
				 </div>
			</div>
		</div>
        </div>
		
	<div class="bg-faded p-4 my-4">
        <hr class="divider">
        <h2 class="text-center text-lg text-uppercase my-0">
          <strong>¿Qué somos?</strong>
        </h2>
        <hr class="divider">
        <p align="center">CVG es una consultora en relaciones que genera espacios reflexivos y que invita a las personas a cambiar la mirada en los distintos espacios relacionales en que 
		se mueven (familia, trabajo, mercado, comunidad). Asimismo, se puede cambiar la mirada de lo que es una negociación, entendiendo que la relación que establecemos define
		cómo queremos llegar al acuerdo.</p>
	</div>
	  <!--
      <div class="bg-faded p-4 my-4">
        <hr class="divider">
        <h2 class="text-center text-lg text-uppercase my-0">Build a website
          <strong>worth visitng</strong>
        </h2>
        <hr class="divider">
        <img class="img-fluid float-left mr-4 d-none d-lg-block" src="img/intro-pic.jpg" alt=""/>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam soluta dolore voluptatem, deleniti dignissimos excepturi veritatis cum hic sunt perferendis ipsum perspiciatis nam officiis sequi atque enim ut! Velit, consectetur.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam pariatur perspiciatis reprehenderit illo et vitae iste provident debitis quos corporis saepe deserunt ad, officia, minima natus molestias assumenda nisi velit?</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit totam libero expedita magni est delectus pariatur aut, aperiam eveniet velit cum possimus, autem voluptas. Eum qui ut quasi voluptate blanditiis?</p>
      </div>

      <div class="bg-faded p-4 my-4">
        <hr class="divider">
        <h2 class="text-center text-lg text-uppercase my-0">Beautiful boxes to
          <strong>showcase your content</strong>
        </h2>
        <hr class="divider">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam soluta dolore voluptatem, deleniti dignissimos excepturi veritatis cum hic sunt perferendis ipsum perspiciatis nam officiis sequi atque enim ut! Velit, consectetur.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam pariatur perspiciatis reprehenderit illo et vitae iste provident debitis quos corporis saepe deserunt ad, officia, minima natus molestias assumenda nisi velit?</p>
      </div>
	  -->
    </div>
    <!-- /.container -->

    <footer class="bg-faded text-center py-5">
      <div class="container">
		<img src="img/convenios/eclass.jpg" width="150" height="50"/><img src="img/convenios/uai.jpg" width="150" height="50"/><img src="img/convenios/conveniomarco.png" width="150" height="50"/>
        <p class="m-0">Convergente 2017 - Diseñado por <a href='http://avanzadatecnologica.cl' target="_blank">Avanzada Tecnológica</a></p>
      </div>
      <br>
      <br>

      <!-- apartado direcciones y contacto con la organizacion -->
      <div class="tagline-lower text-center text-expanded text-shadow text-uppercase text-white mb-5 d-none d-lg-block">RUCAMANQUI 1290 | LO BARNECHEA , SANTIAGO | FONO: +56 2 2217 2028 – MÓVIL: +56 9 9841 3942</div>
    </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>



  <!-- apratado modal de servicios -->

  <!-- modal negociacion relacional -->  
	<div class="portfolio-modal modal fade" id="modal1" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="container">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
				  <img class="d-block img-fluid w-100 h-100" src="img/negociacionRelacional.jpg"/>
                  <h2>Negociación Relacional</h2>
                  <p class="item-intro text-muted">ConVerGente</p>
                  
                  <p>Las discrepancias entre las personas no tienen que ver con los intereses ni la oportunidad, como se nos ha enseñado, sino con las emociones. Nuestro modelo
				  de negociación es una invitación a construir un espacio de relaciones que permita a las partes llegar a acuerdos y abrir un futuro de posibilidades.</p>
				  <p>Porque en la sociedad global que vivimos es imprescindible generar redes de confianza que amplíen el mundo de cada uno de nosotros. Por lo mismo, ConVerGente 
				  muestra las distinciones básicas de una relación – preguntar, escuchar, reflexionar y observar – y refuerza la idea de buscar acuerdos y construir relaciones 
				  basadas en la confianza, el respeto mutuo y en el cumplimiento permanente de las promesas.</p>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Volver</button>
                </div>
          </div>
        </div>
      </div>
    </div>

  <!-- modal relaciones laborales y negociacion -->  
  <div class="portfolio-modal modal fade" id="modal2" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="container">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
          <img class="d-block img-fluid w-100" src="../img/relacionesLaborales.jpg"/>
                  <h2>Relaciones laborales y negociación</h2>
                  <p class="item-intro text-muted">ConVerGente</p>
                  
                  <p>Lo que se viene en materia de relaciones laborales producto de la Reforma Laboral que se discute en el Congreso resulta desafiante para cualquier profesional y dirigente sindical que sea responsable de la gestión laboral.</p>
                  <p>ConVerGente propone una nueva mirada en torno a las relaciones laborales, contingente, innovadora, con sentido de futuro, que explica por qué hemos llegado donde estamos. Invita, además, desde este nuevo camino explicativo, a diseñar el presente con una nueva interpretación de la gestión laboral relacional que consolide un futuro de bienestar para la empresa y trabajadores en general.</p>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Volver</button>
                </div>
          </div>
        </div>
      </div>
    </div>

  <!-- modal negociacion comercial y ventas -->  
  <div class="portfolio-modal modal fade" id="modal3" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="container">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
          <img class="d-block img-fluid w-100" src="img/negociacionComercial.jpg"/>
                  <h2>Negociación comercial y ventas</h2>
                  <p class="item-intro text-muted">ConVerGente</p>
                  
                  <p>A través de este programa, ConVerGente busca impulsar una diferenciación de los ejecutivos centrada en el cliente y en el aumento de su propia productividad con soluciones creativas. Para ello, invitamos a los participantes a reflexionar sobre cómo hacen lo que hacen y a identificar las brechas que impiden o dificultan la obtención de los objetivos de la empresa.</p>
                  <p>Promovemos que los ejecutivos se transformen en lo que llamamos “asesores creativos innovadores”, de manera que cambien su identidad frente a los clientes mediante acciones que generen valor para éstos. Entregamos distinciones para repensar la visión tradicional de gestionar una cuenta y llevarla a su máximo potencial a través de habilidades prácticas que faciliten el desempeño en la venta y negociación.</p>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Volver</button>
                </div>
          </div>
        </div>
      </div>
    </div>

  <!-- modal café relacional -->  
  <div class="portfolio-modal modal fade" id="modal4" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="container">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
          <img class="d-block img-fluid w-100" src="img/cafeRelacional.jpeg"/>
                  <h2>Café relacional</h2>
                  <p class="item-intro text-muted">ConVerGente</p>
                  
                  <p>Con esta dinámica, sostenida en el tiempo, ConVerGente logra que los participantes conversen en estados emocionales que les abran nuevos rumbos y permitan explorar nuevas prácticas y nuevas políticas. Lo anterior, entendiendo la piedra angular del  desarrollo de cualquier organización está en las relaciones laborales y en la confianza en sus equipos de trabajo.</p>
                  <p>En esta dinámica surgen conversaciones que permiten construir una nueva cultura laboral/relacional en la organización. Se posibilita, así, una nueva forma de relacionarse que se basa en la autonomía reflexiva y de acción y que da paso a una mejor comunicación, coordinación, entendimiento y colaboración en los equipos de trabajo.</p>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Volver</button>
                </div>
          </div>
        </div>
      </div>
    </div>

  <!-- modal transformacion cultural -->  
  <div class="portfolio-modal modal fade" id="modal5" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="container">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
          <img class="d-block img-fluid w-100" src="img/transformacionCultural.jpg"/>
                  <h2>Trasformación Cultural</h2>
                  <p class="item-intro text-muted">ConVerGente</p>
                  <p>ConVerGente lleva muchos años mirando y reflexionando sobre nuevos modos de comprender la empresa. Modos o estilos que cambien la mirada cultural tradicional y aporten estados de ánimo refrescantes para construir el presente y el futuro con otro dinamismo.</p>
                  <p>Declaramos que no hay fórmulas ni recetas, que es justamente lo que genera incertidumbre, porque, en general, esperamos que nos digan “cómo se hace”. Sin embargo, ese tiempo terminó. Lo que ofrecemos son reflexiones desde la experiencia práctica con el fin de generar las condiciones para que la organización responda con agilidad y productividad a los distintos presentes que surgen en el convivir y en los varios mercados en que participamos.</p>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Volver</button>
                </div>
          </div>
        </div>
      </div>
    </div>

    <!-- modal conversaciones reflexivas -->  
  <div class="portfolio-modal modal fade" id="modal6" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="container">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
          <img class="d-block img-fluid w-100" src="img/conversacionesReflexivas.png"/>
                  <h2>Conversaciones Reflexivas</h2>
                  <p class="item-intro text-muted">ConVerGente</p>
                  <p>Lo que las personas hoy dejan postergado por las urgencias del día a día es parar un momento, mirar hacia atrás y reflexionar sobre sus experiencias y sus sentires. ConVerGente, a través de este ciclo de conversaciones reflexivas, invita a los participantes a aprender de su propia experiencia a través de la reflexión individual y grupal.</p>
                  <p>La reflexión grupal se lleva a cabo en conversaciones dirigidas por un moderador/a. En ella, los participantes observan más profundamente la manera en que se mueven dentro de los contextos cambiantes y comparten experiencias, opiniones, reflexiones y preguntas con el resto de los participantes. Estas conversaciones se realizan cada 15 días y en cada una se trata un tema diferente (cultural, laboral, contingente) que sea de relevancia para todo el grupo.</p>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Volver</button>
                </div>
          </div>
        </div>
      </div>
    </div>

<?php get_footer(); ?>
