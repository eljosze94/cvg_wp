<?php
/* Template Name: lecturas */ 
get_header('lecturas'); 
// 
$categorias = get_categories( array(
	'orderby' => 'name',
	'post_type' =>'post',
	'order'   => 'ASC'
) );

$url = get_permalink();

?>
<style type="text/css">
.wp-pagenavi a, .wp-pagenavi span {
    border: none;
}
.wp-pagenavi a:hover, .wp-pagenavi span.current {
	font-size: 18px;
    color: #2487c8;
    text-decoration: underline;
}


</style>
<div class="main" role="main">
      <div class="c-inside">
        <div class="container">
          <div class="c-inside__top"> 
            <h1>  <strong>Blue</strong>Blog  </h1>
          </div>
          <div class="c-filter">
            <select class="selectpicker" data-style="link" name="orden" id="select_orden">
              <option value="0">Ordenar Por</option>
              <option value="r">Recientes</option>
              <option value="a">Antiguos</option>
              <option value="p">Populares</option>
            </select>
						<select class="selectpicker" data-style="link" name="categorias" id="select_categorias">
              <option value="0">Filtrar Por</option>
							<?php $count = 0;
										foreach ($categorias as $categoria) { ?>
											<option value="<?php echo $categoria->slug; ?>"><?php echo $categoria->name; ?></option>
							<?php } ?>
            </select>
          </div>

<?php
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$query = array(
                                'showposts' => 5, 
                                'post_type' =>'post', 
                                'posts_per_page' => 5,
																'paged' => $paged,
                                'order'=> 'ASC', 
																'orderby' => 'order');														
			if ($_GET['orden']=='a') {
				$query['order']='ASC';
				$query['orderby']='order';
			}else if ($_GET['orden']=='p') {
				$query['order']='DESC';
				$query['orderby']='meta_value';
				$query['meta_key']= 'post_views_count';
			}else if ($_GET['orden']=='r') {
				$query['order']='DESC';
				$query['orderby']='order';
			}else if ($_GET['categorias']) {
				$query['tax_query']=array(
															array(
																'taxonomy' => 'category',
																'field' => 'slug',
																'terms' => $_GET['categorias'],
															),		
														);
			}else if(!$_GET['orden']){
				$query['order']='DESC';
				$query['orderby']='order';
			}


			//pr($query);
            query_posts($query);
            if ( have_posts() ):
                while (have_posts()) :the_post(); 
                	// pr(get_post_meta($post->ID));
		            echo '
			          	<div class="c-blog -item"> 
			          	  <div class="row">
			          	    <div class="col-md-6"> <img src="'.get_the_post_thumbnail_url($post->ID,'full').'"></div>
			          	    <div class="col-md-6">
			          	      <div class="c-blog__info"><span>Blog</span>
			          	        <h5>'.get_the_title().'</h5>
			          	        <p>'.cortar_palabras(get_the_content(),200).'</p>
			          	        <a class="btn btn-secundary" href="'.get_permalink().'" >Ir al artículo</a>
			          	      </div>
			          	    </div>
			          	  </div>
			          	</div>';
          		endwhile;
          endif;
          ?>
        <div class="c-paginator">
            <?php wp_pagenavi($query); ?>
        </div>
        
      </div>
    </div>


<?php get_footer('resto'); ?>

<script type="text/javascript">
$( document ).ready(function() {
	$('.page-service').addClass('page-blog');
	$('.page-service').removeClass('page-service');
	<?php if($_GET['orden']){ ?>
		$('.c-paginato .wp-pagenavi a').click(function(){
			if ($(this).attr('href')!='#') {
				window.location.href = $(this).attr('href')+'?orden='+<?php echo $_GET['orden']; ?>;
			}
			return false;
		});
	<?php }elseif ($_GET['categorias']) { ?>
		$('.c-paginato .wp-pagenavi a').click(function(){
			if ($(this).attr('href')!='#') {
				window.location.href = $(this).attr('href')+'?categorias='+<?php echo $_GET['categorias']; ?>;
			}
			return false;
		});
	<?php } ?>

	$('#select_orden').change(function(){
		if ($(this).val()!=0) {
			var url      = "<?php echo $url; ?>";
			window.location.href = url+"?orden="+$(this).val();
			return false;
		}
	});

	$('#select_categorias').change(function(){
		var slug_categoria = $(this).val();
		if ($(this).val()!=0) {
			var url      = "<?php echo $url; ?>";
			window.location.href = url+"?categorias="+$(this).val();
			return false;
		}
	});

});
</script>