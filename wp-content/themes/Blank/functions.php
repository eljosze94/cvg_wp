<?php

// Utilizar menus nativos de wordpress
register_nav_menus( array(
	'primary' => __( 'Primary Navigation', 'estudiosuma' ),
) );



/*Genera un orden al listar atributos*/
function pr($data){
	echo '<pre>';
	print_r($data);
	echo '</pre>';
}

/*DIFERENTES OPTIONS PAGES DE ADVANCE CUSTOM FIELDS (ACF PLUGIN) */
if( function_exists('acf_add_options_sub_page') )
{
	acf_add_options_sub_page( 'Home' );
}
add_theme_support( 'post-thumbnails' );
/*CUSTOM POST-TYPE*/
add_action( 'init', 'create_post_type_slider' );
function create_post_type_slider(){
	register_post_type( 'slider',
		array(
			'labels' => array(
				'name' => __( 'Sliders' ),
				'singular_name' => __( 'slider' )
			),
		'public' => true,
		'menu_icon' => 'dashicons-images-alt',
		'has_archive' => true,
		'supports' => array( 'title' )

		)
	);
	register_post_type( 'cliente',
		array(
			'labels' => array(
				'name' => __( 'Clientes' ),
				'singular_name' => __( 'cliente' )
			),
		'public' => true,
		'has_archive' => true,
		'supports' => array( 'title'  )

		)
	);
	// register_post_type( 'blog',
	// 	array(
	// 		'labels' => array(
	// 			'name' => __( 'Blogs' ),
	// 			'singular_name' => __( 'blog' )
	// 		),
	// 	'public' => true,
	// 	'has_archive' => true,
	// 	'supports' => array( 'title','thumbnail','excerpt' )

	// 	)
	// );
}

/*TAXONOMIAS*/
add_action( 'init', 'crear_categorias_slider' );
function crear_categorias_slider() {
	register_taxonomy('slider_categoria','slider',
		array(
			'label' => __( 'Categorias Slider' ),
			'rewrite' => array( 'slug' => 'slider_categoria' ),
			'hierarchical' => true,
		)
	);
	register_taxonomy('cliente_categoria','cliente',
		array(
			'label' => __( 'Categorias Clientes' ),
			'rewrite' => array( 'slug' => 'cliente_categoria' ),
			'hierarchical' => true,
		)
	);
}

//function to display number of posts
	function getPostViews($postID){
	   $count_key = 'post_views_count';
	   $count = get_post_meta($postID, $count_key, true);
	   if($count==''){
	       delete_post_meta($postID, $count_key);
	       add_post_meta($postID, $count_key, '0');
	   }
	   return (int)$count;
	}

	//function to count views
	function setPostViews($postID){
	   $count_key = 'post_views_count';
	   $count = get_post_meta($postID, $count_key, true);
	   if($count==''){
	       delete_post_meta($postID, $count_key);
	       add_post_meta($postID, $count_key, 1);
	   }else{
	       $count++;
	       update_post_meta($postID, $count_key, $count);
	   }
	}
// Register Custom Navigation Walker
require_once('wp_bootstrap_navwalker.php');