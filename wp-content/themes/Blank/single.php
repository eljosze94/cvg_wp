<?php
get_header('resto'); 
$id = get_the_ID();
setPostViews($id);
?>

<div class="main" role="main">
      <div class="c-inside">
        <div class="c-inside__top"> 
          <h1>  <strong>Blue</strong>Blog  </h1>
        </div>
        <?php
        if ( have_posts() ):
                while (have_posts()) :the_post(); ?>
        <div class="c-blog -single"> 
          <div class="container">
            <div class="row">
              <div class="col-md-12"><!-- <span>Opinion</span> -->
                <h2><?php the_title(); ?></h2>
                <div class="c-blog__body img-completo-blog">
                	<?php the_content($id); ?>
                </div>
                <div class="share">
                  <div class="row" style="padding:0">
                    <h1 class="">Comparte</h1>
                  </div>
                  <div class="row" style="padding:0">
                    <div class="col-md-12">
                      <a style="padding-right:1em" href="javascript:void(window.open('http://www.linkedin.com/shareArticle?url='+encodeURIComponent(location),'','width=600,height=400,left=50,top=50'));">
                        <i class="fa fa-linkedin-square fa-3x" aria-hidden="true"></i>
                      </a>
                      <a style="padding-right:1em" href="https://plus.google.com/share?url=<?php echo get_permalink(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=400');return false;">
                        <i class="fa fa-google-plus-square fa-3x" aria-hidden="true"></i>
                      </a>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php endwhile;
          endif; ?>
        <div class="container"> 

          <?php 
          wp_reset_query();
          $x=0;
          $query = array(
                    'showposts' => 1, 
                    'post_type' =>'post', 
                    'posts_per_page' => 1,
                    'paged' => 1,
                    'order'=> 'DESC', 
                    'post__not_in' => array($id ),
                    'orderby' => 'order');
          query_posts($query);
            if ( have_posts() ):

                while (have_posts()) :the_post(); 
                  // pr($post);
                  if ($x==0) {
                  
                  echo '
          <div class="c-blog -item"> 
            <div class="row">
              <div class="col-md-6"> <img src="'.get_the_post_thumbnail_url($post->ID,'full').'"></div>
              <div class="col-md-6">
                <div class="c-blog__info"><span>Blog</span>
                  <h5>'.get_the_title().'</h5>
                  <p>'.cortar_palabras(get_the_content(),200).'</p>
                  <a class="btn btn-secundary" href="'.get_permalink().'">Ir al artículo</a>
                </div>
              </div>
            </div>
          </div>';
        }
          $x++;
            endwhile;
          endif;
          ?>



        </div>
      </div>
    </div>


<?php get_footer('resto'); ?>


<script type="text/javascript">
$( document ).ready(function() {
	$('.page-service').addClass('page-blog');
	$('.page-service').removeClass('page-service');
});
</script>